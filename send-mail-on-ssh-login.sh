[root@wenxiaoxiao pam.d]# cat /usr/local/bin/send-mail-on-ssh-login.sh 
#!/bin/bash

PATH=/bin:/usr/bin
SUBJ="Alert Remote SSH access from pfwu@kabaminc.com"

mail -s $SUBJ << EOF 
ALERT Remote SSH Shell Access (${HOSTNAME}) on $(date)

User ${PAM_USER} logged in from ${PAM_RHOST}
$(who)

EOF